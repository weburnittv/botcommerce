#!/usr/bin/env bash

chown -R www-data var/
chown -R www-data web/mediafile
chmod -R 777 var/

rm /consumers/consumers.ini
touch /consumers/consumers.ini
cp images/php/supervisord/template/consumer.ini /consumers/template/consumer.ini
cp images/php/supervisord/consumer.init.php /consumers/consumer.init.php
cp images/php/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
php -f /consumers/consumer.init.php
cat /consumers/consumers.ini

set -e
supervisord -c /etc/supervisor/conf.d/supervisord.conf