#!/usr/bin/env bash
rm -rf ../autobot-dev
mkdir ../autobot-dev
cp -R app/ ../autobot-dev/
cp -R sr/ ../autobot-dev/
mkdir ../autobot-dev/web
cp web/*.php ../autobot-dev/web/
cp -R web/ ../autobot-dev/
cp -R core_test ../autobot-dev/
cp -R feature ../autobot-dev/
cp -R bin ../autobot-dev/
cp cache.sh ../autobot-dev/
cp composer* ../autobot-dev/

cd ../autobot-dev
composer install
app/console doctrine:cache:clear-query
app/console doctrine:cache:clear-metadata
php app/console assetic:dump
php app/console d:m:migrate -n
./cache.sh dev

chown -R www-data var/
chown -R www-data web/mediafile
chmod -R 777 var/
bin/phpunit -c app/
bin/codeception --config=core_tests/frontend_test/codeception.yml run acceptance --steps