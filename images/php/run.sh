#!/usr/bin/env bash

bower install --allow-root
if [ "$APPLICATION_LIVE" = 1 ]; then
    composer install --no-dev
    composer dump-autoload --optimize --no-dev
    app/console doctrine:cache:clear-query --env=prod
    app/console doctrine:cache:clear-metadata --env=prod
    php app/console d:mi:migrate -n --env=prod
    app/console doctrine:cache:clear-query --env=prod
    app/console doctrine:cache:clear-result --env=prod
    app/console doctrine:cache:clear-metadata --env=prod
else
    composer install
    composer dump-autoload --optimize
    app/console doctrine:cache:clear-query
    app/console doctrine:cache:clear-metadata
    php app/console assetic:dump
    php app/console d:m:migrate -n
    ./cache.sh dev
fi

chown -R www-data var/
chown -R www-data web/mediafile
chmod -R 777 var/

/usr/local/sbin/php-fpm --nodaemonize