var SpikaAdapter = {
    created: false,
    listener : null,
    attach : function(options){
      if(this.created)
      return;
        if(!options)
            return;

        if(!options.user)
            return;
        if(!options.user.id)
            return;
        options.attachTo = 'room-'+options.user.id;
        $('body').append('<div class="frame-chat" id="'+options.attachTo+'">');
        options.user.name = "Anonymous";
        options.user.roomID = options.user.id;

        options.config.apiBaseUrl = "/msg/v1";
        options.config.socketUrl = "/msg";

        window.bootOptions = options;

        // attach to dom
        var iframe = document.createElement('iframe');

        var url = "http://www.neuronese.com/msg";

        url += "?params=" + encodeURIComponent(JSON.stringify(options));
        iframe.src = url;
        iframe.width = '100%';
        iframe.height = '100%';
        iframe.frameBorder = 0;

        var node = document.getElementById(options.attachTo);

        while (node.hasChildNodes()) {
            node.removeChild(node.firstChild);
        }
        node.appendChild(iframe);
        console.log(typeof window.startSpikaIntoDiv);
        console.log(typeof window.tracking);

        if(options.listener)
            this.listener = options.listener;

    }

}

// export to global
window.SpikaAdapter = SpikaAdapter;
