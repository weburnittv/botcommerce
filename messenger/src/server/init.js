(function(global) {
    "use strict;"

    // Class ------------------------------------------------
    var Config = {};

    Config.host = "www.neuronese.com";
    Config.port = 8888;
    Config.urlPrefix = '/msg';
    Config.socketNameSpace = '/msg';

    Config.imageDownloadURL = "http://" + Config.host + "/" + Config.urlPrefix + "/media/images/";
    Config.noavatarImg = "http://" + Config.host + Config.urlPrefix + "/img/noavatar.png";

    Config.chatDatabaseUrl = "mongodb://mongo:27017/messages";
    Config.dbCollectionPrefix = "messaging_";

    Config.uploadDir = 'public/uploads/';
    Config.sendAttendanceMessage = true;

    Config.stickerAPI = '/api/v2/stickers/56e005b1695213295419f5df';

    // Exports ----------------------------------------------
    module["exports"] = Config;

})((this || 0).self || global);
