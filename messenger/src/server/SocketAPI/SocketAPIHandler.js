var _ = require('lodash');

var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var UserModel = require("../Models/UserModel");
var MessageModel = require("../Models/MessageModel");
var Settings = require("../lib/Settings");
var Observer = require("node-observer");
var amqp = require('amqp');

var SocketAPIHandler = {
    connection: amqp.createConnection({
        host: process.env.RABBITMQ_HOST,
        login: process.env.RABBITMQ_USER,
        password: process.env.RABBITMQ_PASSWORD
    }, {defaultExchangeName: "amq.topic"}),
    io: null,
    nsp: null,
    init: function (io) {

        var self = this;
        this.io = io;
        var redis = require("redis");
        rabbitmq = this.connection;
        this.nsp = io.of(Settings.options.socketNameSpace);

        this.nsp.on('connection', function (socket) {
            var sub = redis.createClient(6379, process.env.REDIS_HOST);
            require('./LoginActionHandler').attach(io, socket);
            require('./SendMessageActionHandler').attach(io, socket, rabbitmq);
            require('./DisconnectActionHandler').attach(io, socket, sub);
            require('./SendTypingActionHandler').attach(io, socket);
            require('./OpenMessageActionHandler').attach(io, socket);
            require('./DeleteMessageActionHandler').attach(io, socket);

        });

    }

};

module["exports"] = SocketAPIHandler;
