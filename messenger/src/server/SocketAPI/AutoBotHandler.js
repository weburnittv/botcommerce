var _ = require('lodash');

var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var SocketHandlerBase = require("./SocketHandlerBase");
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
var Settings = require("../lib/Settings");
var request = require('request');
var async = require('async');
var md5 = require("md5");

var AutoBotHandler = function () {

}

var SendMessageLogic = require('../Logics/SendMessage');

var BridgeManager = require('../lib/BridgeManager');

_.extend(AutoBotHandler.prototype, SocketHandlerBase.prototype);

AutoBotHandler.prototype.attach = function (io, socket, subscriber) {

    var self = this;

    var userKey = socket.id.replace(Settings.options.socketNameSpace + '#', '');
    subscriber.subscribe(userKey);
    subscriber.on('message', function (channel, result) {
        console.log(result);
        var data = JSON.parse(result);
        console.log(data);
        var reply = {
            roomID: data.roomID,
            message: data.label,
            userID: "autobot",
            type: Const.messageTypeQuestion,
            localID: md5('autobot-' + Date.now() + result.roomID)
        };
        SendMessageLogic.execute("autobot", reply,
            function (done) {
            },
            function () {

            }
        );
    });
}


module["exports"] = new AutoBotHandler();
