var _ = require('lodash');

var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var SocketHandlerBase = require("./SocketHandlerBase");
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
var request = require('request');
var async = require('async');
var md5 = require("md5");

var SendMessageActionHandler = function () {

}

var SendMessageLogic = require('../Logics/SendMessage');

var BridgeManager = require('../lib/BridgeManager');

_.extend(SendMessageActionHandler.prototype, SocketHandlerBase.prototype);

SendMessageActionHandler.prototype.attach = function (io, socket, rabbitmq) {

    var self = this;

    /**
     * @api {socket} "sendMessage" Send New Message
     * @apiName Send Message
     * @apiGroup Socket
     * @apiDescription Send new message by socket
     * @apiParam {string} roomID Room ID
     * @apiParam {string} userID User ID
     * @apiParam {string} type Message Type. 1:Text 2:File 3:Location
     * @apiParam {string} message Message if type == 1
     * @apiParam {string} fileID File ID if type == 2
     * @apiParam {object} location lat and lng if type == 3
     *
     */

    socket.on('sendMessage', function (param) {


        if (Utils.isEmpty(param.roomID)) {
            socket.emit('socketerror', {code: Const.resCodeSocketSendMessageNoRoomID});
            return;
        }


        if (Utils.isEmpty(param.userID)) {
            socket.emit('socketerror', {code: Const.resCodeSocketSendMessageNoUserId});
            return;
        }

        if (Utils.isEmpty(param.type)) {
            socket.emit('socketerror', {code: Const.resCodeSocketSendMessageNoType});
            return;
        }

        if (param.type == Const.messageTypeText && Utils.isEmpty(param.message)) {
            socket.emit('socketerror', {code: Const.resCodeSocketSendMessageNoMessage});
            return;
        }

        if (param.type == Const.messageTypeLocation && (
            Utils.isEmpty(param.location) ||
            Utils.isEmpty(param.location.lat) ||
            Utils.isEmpty(param.location.lng))) {

            socket.emit('socketerror', {code: Const.resCodeSocketSendMessageNoLocation});

            return;

        }

        async.waterfall(
            [
                function (done) {

                    BridgeManager.hook('sendMessage', param, function (result) {

                        console.log(result);
                        console.log(param);
                        if (result == null || result.canSend) {
                            done(null, param)
                        }

                    });
                },
                function (result, done) {
                    SendMessageLogic.execute(result.userID, result, done(null, result),
                        function (err, code) {
                            if (err) {
                                socket.emit('socketerror', {code: Const.resCodeSocketSendMessageFail});
                            } else {
                                socket.emit('socketerror', {code: code});
                            }
                        });
                }
            ],

            function (err, result) {
                if (err)
                    console.log('Error on sending message');
                else {
                    console.log('Pushing message');
                    result.clientId = socket.id;
                    result.event = 'message:new';
                    rabbitmq.publish('message.new', result, {}, function (data) {
                    });
                }
            })

    });


}


module["exports"] = new SendMessageActionHandler();
